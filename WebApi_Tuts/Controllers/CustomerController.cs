﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_Tuts.Models;

namespace WebApi_Tuts.Controllers
{
    public class CustomerController : ApiController
    {
        public CrudEntities crud = new CrudEntities();
        
        [HttpGet]
        public IEnumerable<Customer> allEmploye()
        {
            return crud.Customers.ToList();
        }

        [HttpGet]

        public HttpResponseMessage pEmploye(int id)
        {
            var data = crud.Customers.Where(x => x.Id == id).FirstOrDefault();
            if(data!=null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Data not found");
            }
        }

        [HttpPost]
        public HttpResponseMessage Insert(Customer customer)
        {
            crud.Customers.Add(customer);
            crud.SaveChanges();
            var data = crud.Customers.Where(x => x.Id == customer.Id).FirstOrDefault();
            if (data != null)
            {
                return Request.CreateResponse(HttpStatusCode.Created, data);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Error");
            }
        }

        [HttpPut]
        public HttpResponseMessage change(int id,Customer customer)
        {
            var data= crud.Customers.Where(x => x.Id == id).FirstOrDefault();
            if(data!=null)
            {
                data.ProductName = customer.ProductName;
                data.Category = customer.Category;
                crud.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, id);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Data Not found");
            }
        }

        [HttpDelete]

        public HttpResponseMessage remove(int id)
        {
            var data = crud.Customers.Where(x => x.Id == id).FirstOrDefault();
            if(data!=null)
            {
                crud.Customers.Remove(data);
                crud.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, "Data Deleted");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Data Not Found");
            }
        }
    }
}
